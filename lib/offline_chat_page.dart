import 'package:flutter/material.dart';

class OfflineChatPage extends StatefulWidget {

  const OfflineChatPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OfflineChatPageState();

}

class _OfflineChatPageState extends State<OfflineChatPage> {

  final TextEditingController _textEditingController = TextEditingController();

  final List<String> _messages = [
    "Сообщение1",
    "Сообщение2",
    "Еще одно сообщение"
  ];

  void _addToList(String text) {
    setState(() {
      _messages.add(text);
    });
    _textEditingController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Оффлайн чат'),
      ),
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    children: _messages.map((message) {
                      return ListTile(
                        title: Text(
                            message
                        ),
                      );
                    }).toList(),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                        child: TextField(
                          controller: _textEditingController,
                        )
                    ),
                    ElevatedButton(
                    child: const Text("Ввести"),
                    onPressed: () {
                      _addToList(_textEditingController.text);
                    },
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}