import 'package:flutter/material.dart';
import 'package:flutter_course/home_page.dart';

import 'main.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        TextFormField(
          decoration: const InputDecoration(
            hintText: 'Введите ваше имя',
          ),
        ),
        ElevatedButton(
          child: const Text('Войти'),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => MyHomePage(key: key,),));
          },
        )
      ],
    );
  }
}
