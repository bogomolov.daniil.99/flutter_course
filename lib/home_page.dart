import 'package:flutter/material.dart';
import 'package:flutter_course/offline_chat_page.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  final String title = 'Flutter course HW';

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: ListView(
            children: [
              ListTile(
                title: const Text('HW1'),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (builder) => const OfflineChatPage()
                  ));
                },
              )
            ]
          ),
        ));
  }
}
